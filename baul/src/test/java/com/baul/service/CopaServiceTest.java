package com.baul.service;

import com.baul.BaulApplication;
import com.baul.domain.Equipo;
import java.util.List;
import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = BaulApplication.class)
@ActiveProfiles("test")
@WebAppConfiguration
public class CopaServiceTest {
    
    @Autowired
    private CopaService copaService;
    
    @Test
    public void obtenerEquiposPorGrupo_conGrupoExistente_devuelveListaEquipos () {
        
        List<Equipo> equiposGrupoA = copaService.obtenerEquiposPorGrupo("A");
    
        assertNotNull(equiposGrupoA);
    }
    
}
