package com.baul;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BaulApplication {

    public static void main(String[] args) {
        SpringApplication.run(BaulApplication.class, args);
    }
}
